# MinetestLearningMods

**Name : Minetest-learning-mods-list**

This is a list of mods related to learning either Minetest, or generally.

**Themes list :**
- architecture
- science (biology, geology, mathematics, physics, paleo)
- history
- cartography
- economy
- literature
- technology
- programming
- general (all mods for all studs)

**Number of mods listed : 81**

**Author of this list :** *Sangokuss, 2020.*

**Contact :** *Frederic.veron@framasoft.org*

##############################################################################

# MinetestLearningMods_Fr

**Nom : Minetest-learning-mods-list**

Ceci est une liste des mods Minetest d'intérêts pédagogiques, classés par disciplines.

**Liste des disciplines :**
- architecture
- science (biologie, géologie, mathématiques, sciences physiques, paleologie)
- histoire (et archéologie)
- cartographie, générateur de monde
- économie
- lettres, apprentissage de la langue
- technologies
- programmation
- général (mods utilisables dans toutes les disciplines)

**Nombre de mods listés : 81**

**Auteur principal :** * Sangokuss, 2020*

**Contact :** *Frederideric.veron@framasoft.org*